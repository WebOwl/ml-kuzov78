$(document).ready(function(){

	$(window).load(function () {
        let phones = [
            {'mask': '+7 \\ \\ ###-###-##-##'}
        ];

        $('input[type=tel]').inputmask({
            mask: phones,
            greedy: false,
            definitions: {
                '#':
                    {
                        validator: '[0-9]',
                        cardinality: 1
                    }
            }
        });
    });

    /*var navPos = $('nav').offset().top;

    $(window).scroll(function() {

        winPos = $(window).scrollTop();
      
      if (winPos >= navPos) {
        $('nav').addClass('fixed shadow');  
        $('.clone-nav').show();
      }  
      else {
        $('nav').removeClass('fixed shadow');
        $('.clone-nav').hide();
      }
    });*/

	jQuery(".top_menu li a").click(function () {
        elementClick = jQuery(this).attr("href")
        destination = jQuery(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 700);
        return false;
    });

    $('.photos a').fancybox({

    });

    var width = $(window).width();

     if ((width > '319') && (width < '1381')) {
        $('.photos ').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            focusOnSelect: true,
            variableWidth: true
        });
     }

      $(document).on('af_complete', function(event, response) {
        if(response.success) $.fancybox.close();
    })

});