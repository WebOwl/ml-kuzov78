<?php
    include('config.php');
    $specialid = $_GET['id'];
    if(!$specialid || !isset($specials[$specialid]))
        header("Location: /");
    $special = $specials[$specialid];
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кузовной ремонт и покраска Вашего авто в Санкт-Петербурге</title>
    <meta name="Description" content="Кузовной ремонт и покраска автомобилей в Санкт-Петербурге">
    <meta name="Keywords" content="">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <link rel="stylesheet" href="/assets/css/normalize.css" type="text/css" />
    <link rel="stylesheet" href="/assets/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="/assets/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="/assets/css/jquery.fancybox.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/style.css" type="text/css" />
    <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PHCHNGS');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHCHNGS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <header class="header">
            <div class="phone_mob"></div>
            <div class="container">
                <a href="/" class="logo"><img src="/assets/img/logo.svg" alt=""></a>
                <div class="logo_text">Кузовной ремонт<br> и покраска автомобилей</div>
                <div class="address">Санкт-Петербург,<br> Ириновский пр., 10 А</div>
                <div class="phone">
                    <a href="tel:+78126001800" class="clearfix"><sub>+7 812</sub>600 18 00</a>
                    <span>Работаем с 9:00 до 20:00</span>
                </div>
                <a href="#" class="btn callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на сервис</a>
            </div>
        </header>
    </div>
       <div class="container">
        <div class="breadcrumbs">
                <ul>
                    <li><a href="/">Главная</a></li>
                    <li>></li>
                    <li>
                        <?=$special['title'];?>
                    </li>
                </ul>
            </div>
    <div class="banners">  
        <img src="/specials/banners/<?=$special['banner'];?>" alt="" />
    </div>
    
 
            
            <div class="title">
                    <?=$special['title'];?>
                </div>
            <div>
                <div class="content">
                    <?=$special['content'];?>
                </div>
            </div>
            <div class="container footnote">
                <?=$special['disclaimer'];?>
            </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="specialPhone">Цены и условия по спецпредложениям и акциям уточняйте по тел. <a href="tel:+78126001800">+7(812)600-18-00</a></div>
        <div class="footer_top">

                <a href="/" class="logo"><img src="/assets/img/logo.svg" alt=""></a>
                <div class="logo_text">Кузовной ремонт<br> и покраска автомобилей</div>
                <div class="address">Санкт-Петербург,<br> Ириновский пр., 10 А</div>
                <div class="phone">
                    <a href="tel:+78126001800" class="clearfix"><sub>+7 812</sub>600 18 00</a>
                    <span>Работаем с 9:00 до 20:00</span>
                </div>
                <a href="#" class="btn callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на сервис</a>

        </div>
        <div class="footer_bottom">

                <div class="copyright">2021</div>
                <a href="https://autodrive-agency.ru/" target="_blank" class="promo">Разработка сайта — <span>avtodrive/agency</span>
                    <a href="#" class="policy">Политика конфиденциальности</a>
                    <a href="#" class="policy">Пользовательское соглашение</a>
                    <a href="/source/pdfs/privacy_policy_2021.pdf" class="privacy" target="_blank">Согласие на обработку персональных данных и получение рекламы</a>
            </div>

        <div class="footnote">
            Информация на сайте, в том числе цены на кузовной ремонт автомобиля и другие работы, носит исключительно ознакомительный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 п.2 Гражданского кодекса РФ. Стоимость кузовного ремонта и расходных материалов может меняться в зависимости от марки автомобиля, его возраста, пробега, технического состояния и характера повреждения.<br>Стоимость краски может изменяться в зависимости от разницы в стоимости пигментов на каждый рецепт краски. Цена работы по покраске двери автомобиля и других деталей рассчитывается индивидуально в каждом конкретном случае.
        </div>
</div>
    </footer>
    </div>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
    <script type="text/javascript" src="/assets/js/calltouch.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="/assets/js/slick.min.js"></script>
    <script type="text/javascript" src="/assets/js/frontend.js"></script>
</body>

</html>