<?php include('specials/config.php');?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кузовной ремонт и покраска Вашего авто в Санкт-Петербурге</title>
    <meta name="Description" content="Кузовной ремонт и покраска автомобилей в Санкт-Петербурге">
    <meta name="Keywords" content="">
    <!--  <base href="http://ml-kuzov78.ru/"> -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet" href="./assets/css/normalize.css" type="text/css" />
    <link rel="stylesheet" href="./assets/css/slick.css" type="text/css" />
    <link rel="stylesheet" href="./assets/css/slick-theme.css" type="text/css" />
    <link rel="stylesheet" href="./assets/css/jquery.fancybox.css" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/style.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Google Tag Manager -->
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PHCHNGS');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function(m, e, t, r, i, k, a) {
        m[i] = m[i] || function() {
            (m[i].a = m[i].a || []).push(arguments) };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(71733040, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/71733040" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHCHNGS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <header class="header">
            <div class="container">
                <a href="/" class="logo"><img src="assets/img/logo.svg" alt=""></a>
                <div class="logo_text">Кузовной ремонт<br> и покраска автомобилей</div>
                <div class="address">Санкт-Петербург,<br> Ириновский пр., 10 А<br>
                <a href="#contacts"><span>Посмотреть карту проезда</span></a>
            </div>
                <div class="phone">
                    <a href="tel:+78126001800" class="clearfix"><sub>+7 812</sub>600 18 00</a>
                    <span>Работаем с 9:00 до 20:00</span>
                </div>
                <a href="#" class="btn callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на сервис</a>
                <div class="phone_mob"><a href="#" class="phone_mob callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;"></a></div>
            </div>
        </header>
       <section class="menu__wrap">
    <div class="container">
        <nav>
        <ul class="menu top_menu">
            <li class="first"><a href="#actions" title="Акции">Акции</a></li>
            <li><a href="#services" title="Услуги">Услуги</a></li>
            <li><a href="#about" title="О нас">О нас</a></li>
            <li><a href="#contacts" title="Контакты">Контакты</a></li>
            <li><a href="#callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на бесплатную оценку</a></li>
        </ul>
        <input type="checkbox" id="btn__open" class="menu__ticker">
        <label class="btn__menu" for="btn__open">
            <span class="first"></span>
            <span class="second"></span>
            <span class="third"></span>
        </label>
        <ul class="hidden__menu">
            <li class="first"><a href="#actions" title="Акции">Акции</a></li>
            <li><a href="#services" title="Услуги">Услуги</a></li>
            <li><a href="#about" title="О нас">О нас</a></li>
            <li><a href="#contacts" title="Контакты">Контакты</a></li>
            <li><a href="#callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на бесплатную оценку</a></li>
        </ul>
        </nav>
    </div>
</section>
    <div class="panorama"></div>
    <div class="top_wrap">
        <div class="container">
            <div class="title">Кузовной ремонт <br>
                и покраска вашего авто <br>
                В Санкт-Петербурге</div>
            <div class="top_wrap_auto"></div>
            <div class="advantages">
                <div class="advantages_item">
                    Точный <br>подбор <br>цвета
                </div>
                <div class="advantages_item">
                    Сертифицированный<br> персонал
                </div>
                <div class="advantages_item">
                    Профессиональное<br> оборудование
                </div>
            </div>
            <a href="#" class="red_btn" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на сервис</a>
            <div class="text_sale_price">Бесплатно оценим стоимость<br> ремонта вашего авто</div>
        </div>
        
    </div>
    <div class="banner" id="action">
        <div class="container">
            <img src="assets/img/banner.png" alt="">
        </div>
    </div>
    <div id="services"></div>
    <section class="services__wrap">
        <div class="container-service">
            <div class="title">Наши Услуги</div>
            <div class="services__items">
                <div class="services__items-row">
                    <a class="services__item service1" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Ремонт кузова</div>
                        <div class="service__text">Абсолютно любой сложности, доверьте нам ваш автомобиль и заберите его словно из салона.</div>
                        <div class="price service__price">
                            <div class="price__info">от 1500 <span class="rouble">j</span></div>
                        </div>
                    </a>
                    <a class="services__item service2" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Покраска</div>
                        <div class="service__text">Восстановим лакокрасочное покрытие вашего автомобиля. Покрасим необходимые элементы цвет в цвет.</div>
                        <div class="price service__price">
                            <div class="price__info">от 3500 <span class="rouble">j</span></div>
                        </div>
                    </a>
                    <a class="services__item service3" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Полировка</div>
                        <div class="service__text">Наши мастера с огромным опытом восстановят блеск вашего автомобиля. Ваше авто засияет новизной.</div>
                        <div class="price service__price">
                            <div class="price__info">от 960 <span class="rouble">j</span></div>
                        </div>
                    </a>
                </div>
                <div class="services__items-row">
                    <a class="services__item service4" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Защитная пленка</div>
                        <div class="service__text">Новый внешний вид вашего автомобиля за небольшую цену. Не оставит вас без внимания и защитит покрытие.</div>
                        <div class="price service__price">
                            <div class="price__info">от 9600 <span class="rouble">j</span></div>
                        </div>
                    </a>
                    <!-- <a class="services__item service5" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                            <div class="service__name">Химчистка</div>
                            <div class="service__text">Мотор ленд на страже чистоты вашего автомобиля. Профессиональная чистка - идеальный результат.</div>
                            <div class="price service__price"><div class="price__info">от 3000 <span class="rouble">j</span></div></div>
                        </a> -->
                    <!-- <a class="services__item service6" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                            <div class="service__name">Ремонт вмятин без покраски</div>
                            <div class="service__text">Профессионально удалим вмятины на кузове вашего автомобиля. Кузов станет как у нового авто.</div>
                            <div class="price service__price"><div class="price__info">от 1500 <span class="rouble">j</span></div></div>
                        </a> -->
                    <a class="services__item service4" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Замена лобового стекла</div>
                        <div class="service__text">Профессионально и быстро заменим лобовое стекло. Лучшая цена и качество.</div>
                        <div class="price service__price">
                            <div class="price__info">от 9600 <span class="rouble">j</span></div>
                        </div>
                    </a> <a class="services__item service4" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Полировка фар</div>
                        <div class="service__text">Верните машине «чистый взгляд». Это не только сделает автомобиль более привлекательным, но и безопасным.</div>
                        <div class="price service__price">
                            <div class="price__info">от 600 <span class="rouble">j</span></div>
                        </div>
                    </a>
                </div>
                <div class="services__items-row">
                    <a class="services__item service1 service9" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name">Ремонт бамперов</div>
                        <div class="service__text">Устраним заломы и царапины.<br /> Быстро, качественно, доступно!<br /><br /></div>
                        <div class="price service__price">
                            <div class="price__info">от 1200 <span class="rouble">j</span></div>
                        </div>
                    </a>
                    <a class="services__item extra" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name"></div>
                        <div class="service__text"></div>
                        <div class="price service__price">
                            <div class="price__info"></div>
                        </div>
                    </a>
                    <a class="services__item extra" href="#" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">
                        <div class="service__name"></div>
                        <div class="service__text"></div>
                        <div class="price service__price">
                            <div class="price__info"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div style="display:none;" class="services" id="services">
            <div class="container">
                <div class="services_items_box">
                    <div class="services_items">
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/1.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Ремонт кузова</div>
                            <div class="service_item_desc">Абсолютно любой сложности, доверьте нам ваш автомобиль и заберите его словно<br> из салона.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/2.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Покраска</div>
                            <div class="service_item_desc">Восстановим лакокрасочное покрытие вашего автомобиля. Покрасим необходимые элементы цвет в цвет.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/3.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Полировка</div>
                            <div class="service_item_desc">Наши мастера с огромным опытом восстановят блеск вашего автомобиля. Ваше авто засияет новизной.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                    </div>
                </div>
                <div class="services_items_box">
                    <div class="services_items">
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/4.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Защитная пленка</div>
                            <div class="service_item_desc">Новый внешний вид вашего автомобиля<br> за небольшую цену. Не оставит вас без<br> внимания и защитит покрытие.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/5.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Химчистка</div>
                            <div class="service_item_desc">Мотор ленд на страже чистоты вашего автомобиля. Профессиональная чистка — идеальный результат.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/6.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Обработка кузова защитным покрытием Нанокерамика</div>
                            <div class="service_item_desc">Мы надежно защитим кузов вашего автомобиля от царапин и сохраним его блеск на несколько лет.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                    </div>
                </div>
                <div class="services_items_box">
                    <div class="services_items">
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/7.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Замена лобового стекла</div>
                            <div class="service_item_desc">Профессионально и быстро заменим лобовое стекло. Лучшая цена и качество<br> в Санкт-Петербурге.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/8.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Полировка фар</div>
                            <div class="service_item_desc">Верните машине «чистый взгляд». Это не только сделает автомобиль более привлекательным, но и безопасным.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                        <a href="#" class="service_item" onclick="window.Calltouch.Callback.onClickCallButton();">
                            <div class="service_item_img" style="background-image: url(./assets/img/9.jpg); ">
                                <!-- <div class="service_item_price"><b>от</b> 9 600 <i>o</i></div> -->
                            </div>
                            <div class="service_item_title">Ремонт бамперов</div>
                            <div class="service_item_desc">Устраним заломы и царапины.</div>
                            <span class="btn">Запись на бесплатный осмотр</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="car_items">
            <div class="car_items_img"></div>
            <div class="container">
                <div class="car">
                    <div class="car_price_first_line">
                        <div class="car_item_price">
                            Покраска зеркала<br> <span>от 3 500 ₽</span>
                        </div>
                        <div class="car_item_price">
                            Покраска крыш<br> <span>от 10 000 ₽</span>
                        </div>
                        <div class="car_item_price extra">
                            <!-- <span>Замена заднего <br>стекла</span> от  <i>o</i> -->
                        </div>
                        <div class="car_item_price">
                            Покраска крышки<br>багажника <span>от 8 500 ₽</span>
                        </div>
                    </div>
                    <div class="car_price_second_line clearfix">
                        <div class="car_item_price extra">
                            <!-- <span>Замена лобового <br>стекла</span> от  <i>o</i> -->
                        </div>
                        <div class="car_item_price">
                            Покраска крыла<br><span>от 8 500 ₽</span>
                        </div>
                    </div>
                    <div class="car_price_third_line clearfix">
                        <div class="car_item_price">
                            Покраска капота </span><br><span>от 9 500 ₽</span> 
                        </div>
                        <div class="car_item_price">
                            Покраска бампера<br><span>от 8 500 ₽</span>
                        </div>
                    </div>
                    <div class="car_price_fouth_line clearfix">
                        <div class="car_item_price extra">
                           <!-- <span>
                                 Полировка фар<br> от  <i>o</i></span> -->
                        </div>
                        <div class="car_item_price">
                            Покраска двери<br><span>от 8 500 ₽</span>
                        </div>
                        <div class="car_item_price extra">
                            <!-- <span>Покраска ручки<br> двери</span> от  <i>o</i> -->
                        </div>
                        <div class="car_item_price car_item_price_last">
                            Покраска порога<br><span>от 3 500 ₽</span> 
                        </div>
                        <img src="assets/img/ml_logo.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="about" id="about">
            <div class="container">
                <div class="title">Кузовной цех<br> мотор ленд</div>
                <div class="desc">Специалисты автоцентра МОТОР ЛЕНД выполняют кузовной ремонт любой сложности. Мастерство наших квалифицированных специалистов и технологичное оборудование позволяет нам предлагать комплексные решения для восстановления автомобилей любых марок и моделей. Приезжайте и получите качественный кузовной ремонт автомобиля в Санкт-Петербурге с гарантией.</div>
                <div class="photos clearfix">
                    <a data-fancybox="photos" href="assets/img/img1.png" style="background: url(./assets/img/img1.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img2.png" style="background: url(./assets/img/img2.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img3.png" style="background: url(./assets/img/img3.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img4.png" style="background: url(./assets/img/img4.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img1.png" style="background: url(./assets/img/img1.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img2.png" style="background: url(./assets/img/img2.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img3.png" style="background: url(./assets/img/img3.png);"></a>
                    <a data-fancybox="photos" href="assets/img/img4.png" style="background: url(./assets/img/img4.png);"></a>
                </div>
            </div>
        </div>
        </div>
        <div class="free_consult">
            <div class="master"></div>
            <div class="container">
                <div class="free_consult_text">
                    <div class="title">Запишитесь<br> на бесплатную<br> оценку стоимости<br> ремонта</div>
                    <div class="desc">Мы бесплатно оценим стоимость ремонта вашего авто.<br> Предложим разные варианты</div>
                    <a href="#" class="black_btn" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на сервис</a>
                </div>
                <div class="free_consult_adv">
                    <div class="free_consult_adv_item">
                        Точный <br>подбор <br>цвета
                    </div>
                    <div class="free_consult_adv_item">
                        Сертифицированный<br> персонал
                    </div>
                    <div class="free_consult_adv_item">
                        Профессиональное<br> оборудование
                    </div>
                </div>
                <div class="text_mob">Бесплатно оценим стоимость ремонта вашего авто</div>
            </div>
            
        </div>
        <section class="specials">
            <div id="actions"></div>
            <div class="container">
                <div class="title">Акции</div>
                <div class="list">
                    <?php foreach($specials as $special => $item): ?>
                    <div class="item">
                        <img src="/specials/banners/<?= $item['banner']; ?>" />
                        <div class="content">
                            <p class="title">
                                <?= $item['title']; ?>
                            </p>
                            <span class="footnote">
                                <?= $item['disclaimer']; ?></span>
                            <a href="#" class="btn" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Узнать подробности</a>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
        </section>
        </div>
        <footer class="footer" id="contacts">
            <div class="container">
                <div class="title">Контакты</div>
                <div class="map">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9bbfdfacad0c4c7a7e7ed5da731197cf2df59a07f9a360cbd154fabbf4ab9346&amp;width=500&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
            <!-- <div class="container">
                <div class="specialPhone">Цены и условия по спецпредложениям и акциям уточняйте по тел. <a href="tel:+78126001800">+7(812)600-18-00</a></div>
            </div> -->
            <div class="footer_top">
                <div class="container">
                    <a href="/" class="logo"><img src="assets/img/logo.svg" alt=""></a>
                    <div class="logo_text">Кузовной ремонт<br> и покраска автомобилей</div>
                    <div class="address">Санкт-Петербург,<br> Ириновский пр., 10 А</div>
                    <div class="phone">
                        <a href="tel:+78126001800" class="clearfix"><sub>+7 812</sub>600 18 00</a>
                        <span>Работаем с 9:00 до 20:00</span>
                    </div>
                    <a href="#" class="red_btn callback" onclick="window.Calltouch.Callback.onClickCallButton(); return false;">Запись на сервис</a>
                </div>
            </div>
            <div class="container">
                <div class="footer_bottom">
                    <div class="copyright">2021</div>
                    <a href="https://autodrive-agency.ru/" target="_blank" class="promo">Разработка сайта — <span>avtodrive/agency</span>
                        <a href="../source/pdfs/privacy_policy_2021.pdf" class="privacy" target="_blank">Согласие на обработку персональных данных и получение рекламы</a>
                </div>
            </div>
            <div class="container footnote">
                Информация на сайте, в том числе цены на кузовной ремонт автомобиля и другие работы, носит исключительно ознакомительный характер и ни при каких условиях не является публичной офертой, определяемой положениями статьи 437 п.2 Гражданского кодекса РФ. Стоимость кузовного ремонта и расходных материалов может меняться в зависимости от марки автомобиля, его возраста, пробега, технического состояния и характера повреждения.<br>Стоимость краски может изменяться в зависимости от разницы в стоимости пигментов на каждый рецепт краски. Цена работы по покраске двери автомобиля и других деталей рассчитывается индивидуально в каждом конкретном случае.
            </div>
        </footer>
        </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.62/jquery.inputmask.bundle.js"></script>
        <script type="text/javascript" src="./assets/js/jquery.fancybox.min.js"></script>
        <script type="text/javascript" src="./assets/js/calltouch.js"></script>
        <script type="text/javascript" src="./assets/js/slick.min.js"></script>
        <script type="text/javascript" src="./assets/js/frontend.js"></script>
        <script type="text/javascript" src="./assets/js/menu.js"></script>
</body>

</html>